/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Fraccion;
import Vista.Test_punto2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author madarme
 */
public class MatrizFraccion {

    private SistemaFraccion[] fracciones;

    public MatrizFraccion() {
    }

    /**
     * 
     * @param args
     */   
      public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tamaño de la matriz: ");

        int n = 0;
        int m = 0;
        Integer[][] arr = new Integer[n][m];

        try {
             n = sc.nextInt();
            arr = new Integer[n][m];
            System.out.println("Ingrese los elementos de la matriz: ");
            for (int i = 0; i < n; i++) {
                 int j = 0;
                arr[i][j] = sc.nextInt();
            }
} catch (Exception e) {
            System.out.println(arr);
        }
    


            
       

    }
  
    public MatrizFraccion(SistemaFraccion[] fracciones) {
        this.fracciones = fracciones;
    }
    
    

    /**
     * Constructor de Matriz de tamaño rectangular o cuadrada
     *
     * @param n tamaño de filas
     * @param m tamaño de columnas
     */
    public MatrizFraccion(int n, int m) {
        validar(n);
        validar(m);
        this.fracciones = new SistemaFraccion[n];
        for (int i = 0; i < n; i++) {
            this.fracciones[i] = new SistemaFraccion(m);
        }
    }

    /**
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param num numerador de la fracción
     * @param den denominador de la fracción
     */
    public void set(int i, int j, float num, float den) {
        this.validarFila(i);
        try {
            this.fracciones[i].set(j, num, den);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Fraccion get(int i, int j) {
        this.validarFila(i);
        try {
            return this.fracciones[i].get(j);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void validarFila(int i) {
        if (this.fracciones == null || i < 0 || i >= this.fracciones.length) {
            throw new RuntimeException("índice de fila fuera de rango:" + i);
        }

    }

    private void validar(int i) {
        if (i < 0) {
            throw new RuntimeException("Error tamaño de matriz");
        }

    }

    @Override
    public String toString() {
        String msg = "";
        for (SistemaFraccion f : this.fracciones) {
            msg += f.toString() + "\n";
        }
        return msg;

    }

    public int getTamFilas() {
        if (this.fracciones == null) {
            throw new RuntimeException("Error matriz vacía");
        }

        return this.fracciones.length;
    }

    public int getTamColumnas() {
        if (this.fracciones == null) {
            throw new RuntimeException("Error matriz vacía");
        }
        return this.fracciones[0].length();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Arrays.deepHashCode(this.fracciones);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MatrizFraccion other = (MatrizFraccion) obj;

        return true;
    }

    public String getNoSeRepite() {
        return ":)";
    }

    /**
     * Crea una matriz con fracciones randómicas
     */
    public void crearRandom() {

    }

}
